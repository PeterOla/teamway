export type QuestionProps = {
  question: string;
  options: Array<String>;
  answer: "A" | "B" | "C" | "D";
};
