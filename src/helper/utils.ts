export const numberToLetter = (value: number) => {
  const adjusted = (Math.round(Math.abs(value)) % 26) + 1;
  return (adjusted + 9).toString(36).toUpperCase();
};
