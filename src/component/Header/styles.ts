import styled from "styled-components";
import { colors } from "../../styles/global";

type StyledBarProps = {
  color?: string;
  active: boolean;
};

export const StyledHeader = styled.div`
  display: flex;
  flex-direction: row;
  margin: 24px;
`;

export const StyledBar = styled.div<StyledBarProps>`
  height: 4px;
  flex: 1;
  border-radius: 16px;
  margin: 0px 3px;
  background-color: ${(props) => (props.active ? colors.primary : colors.gray)};
  transition: all 0.5s;
`;
