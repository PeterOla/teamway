import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { StyledBar, StyledHeader } from "./styles";

const Header: React.FC = () => {
  const { progress, questions } = useSelector(
    (state: RootState) => state.question
  );

  return (
    <StyledHeader>
      {questions.map((x, i) => (
        <StyledBar key={i} active={progress >= i} />
      ))}
    </StyledHeader>
  );
};

export default Header;
