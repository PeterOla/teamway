import styled, { keyframes } from "styled-components";
import { colors } from "../styles/global";

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Spinner = styled.div`
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);

  border-top: 2px solid ${colors.gray};
  border-right: 2px solid ${colors.gray};
  border-bottom: 2px solid ${colors.gray};
  border-left: 2px solid ${colors.primary};
  background: transparent;
  width: 24px;
  height: 24px;
  border-radius: 50%;
`;

export default Spinner;
