import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { numberToLetter } from "../../helper/utils";
import { useAppDispatch } from "../../redux/hooks";
import {
  nextQuestion,
  prevQuestion,
  setSelectedAnswer,
} from "../../redux/questionSlice";
import { RootState } from "../../redux/store";
import {
  Spacer,
  SpacerHorizontal,
  StyledFlex,
  StyledH4,
  StyledP,
} from "../../styles/global";
import { Button } from "../Button";
import Spinner from "../Spinner";
import {
  Container,
  OptionIndicator,
  QuestionContainer,
  StyledOption,
} from "./styles";

const Main: React.FC = () => {
  const dispatch = useAppDispatch();
  let navigate = useNavigate();

  const { progress, questions, selectedAnswer } = useSelector(
    (state: RootState) => state.question
  );

  // Getting Question according to the current progress position
  const currentQuestion = questions[progress]?.question;
  // Getting Options according to the current progress position
  const currentOption = questions[progress]?.options;
  // Checking if we're currently at the last Question
  const onLastQuestion = progress === questions.length - 1;

  return (
    <Container>
      {questions.length === 0 ? (
        // Placeholder spinner while mock data is feteching
        <Spinner data-testid={"question-loading"} />
      ) : (
        <QuestionContainer data-testid={"question-container"}>
          <StyledP>
            {/* Question Tracker */}
            Question {progress + 1}/{questions.length}
          </StyledP>

          <StyledH4>{currentQuestion}</StyledH4>
          <StyledP fontStyle="italic">All questions are required</StyledP>

          <Spacer />

          {currentOption.map((option_item, i) => {
            const isSelected = selectedAnswer === i;

            return (
              <StyledOption
                data-testid={"question-option" + i}
                // If the right answer was selected pass approprate props to
                selected={isSelected}
                key={i}
                // OnClick saves selected option into store, which in-turn activates the Next Button.
                onClick={() => dispatch(setSelectedAnswer(i))}
              >
                <OptionIndicator selected={isSelected}>
                  <StyledP light={isSelected}>{numberToLetter(i)}</StyledP>
                </OptionIndicator>

                <StyledP fontSize="14px">{option_item}</StyledP>
              </StyledOption>
            );
          })}

          <Spacer />

          <StyledFlex>
            {/* Show button "Previous" if the we have a previous to go to. (i.e Question 2+) */}
            {progress > 0 && (
              <>
                <Button
                  data-testid={"previous-btn"}
                  onClick={() => dispatch(prevQuestion())}
                >
                  Previous
                </Button>
                <SpacerHorizontal />
              </>
            )}

            <Button
              data-testid={"next-btn"}
              // Disabled this button if we have not selected any option
              disabled={selectedAnswer === undefined || selectedAnswer === -1}
              // Alterate button style *filled
              primary
              // Change button color to secondary theme color if we're on the last Question
              secondary={onLastQuestion}
              onClick={() => {
                dispatch(nextQuestion());

                // If we're on the last Question, Navigate to Result screen
                if (onLastQuestion) navigate("/result");
              }}
            >
              {/* Switch button text to Finish Test on last Question  */}
              {onLastQuestion ? "Finish Test" : "Next Question"}
            </Button>
          </StyledFlex>
        </QuestionContainer>
      )}
    </Container>
  );
};

export default Main;
