import styled from "styled-components";
import { device } from "../../styles/breakpoints";
import { colors } from "../../styles/global";

export const Container = styled.div`
  height: 90vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const QuestionContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  margin: 20px 20px;
  padding: 5vh 5vw;
  align-items: center;
  justify-content: center;
  background-color: ${colors.gray_40};
  transition: all 0.5s;
  @media ${device.mobileM} {
    margin: 0px;
  }
`;

type StyledOptionProps = {
  selected?: boolean;
};

export const StyledOption = styled.div<StyledOptionProps>`
  display: flex;
  border: ${(props) =>
    props.selected
      ? `1px solid ${colors.secondary}`
      : `1px solid ${colors.gray_80}`};
  border-radius: 6px;
  width: 100%;
  background-color: white;
  padding: 14px 10px;
  margin-bottom: 16px;
  align-items: center;
  cursor: pointer;
  &:hover {
    border: 1px solid ${colors.secondary};
  }

  transition: all 0.5s;
`;

export const OptionIndicator = styled.div<StyledOptionProps>`
  background-color: ${(props) =>
    props.selected ? `${colors.secondary}` : `${colors.gray_40}`};
  padding: 4px 8px;
  margin: 0px 12px 0px 4px;
  border-radius: 10%;
  transition: all 0.5s;
`;
