import { render } from "@testing-library/react";
import React from "react";
import { Button } from "../Button";
import Spinner from "../Spinner";

describe("Components renders with crashing", () => {
  it("Button renders successfully", () => {
    render(<Button />);
  });

  it("Spinner renders successfully", () => {
    render(<Spinner />);
  });
});
