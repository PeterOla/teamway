import styled from "styled-components";
import { colors } from "../styles/global";

type BtnProps = {
  autoFlex?: boolean;
  width?: string;
  primary?: boolean;
  secondary?: boolean;
  margin?: string;
};

export const Button = styled.button<BtnProps>`
  flex: ${(props) => (props.autoFlex ? null : 1)};
  background: ${(props) =>
    props.secondary
      ? colors.secondary
      : props.primary
      ? colors.primary
      : colors.white};
  color: ${(props) => (props.primary ? colors.white : colors.gray_100)};
  margin: ${(props) => props.margin || "0px"};
  width: ${(props) => props.width || "auto"};
  border: ${(props) =>
    props.primary ? "none" : `1px solid ${colors.primary}`};
  padding: 7px 12px;
  border-radius: 4px;
  height: 52px;
  font-weight: bold;
  cursor: pointer;
  justify-content: center;

  &:disabled {
    background: ${colors.gray_80};
  }
  &:hover {
    opacity: ${(props) => (props.primary ? 0.7 : 1)};
    color: ${(props) => (props.primary ? colors.white : colors.secondary)};
    border: ${(props) =>
      props.primary ? "none" : `1px solid ${colors.secondary}`};
  }
  transition: all 0.5s;
`;
