import { QuestionProps } from "../helper/interface";
import { QUESTION } from "../helper/question";
import { numberToLetter } from "../helper/utils";

export function fetchQuestion() {
  return new Promise<QuestionProps[]>((resolve) =>
    setTimeout(() => resolve(QUESTION), 1000)
  );
}

export function calculateResult(answers: number[], questions: QuestionProps[]) {
  return new Promise<number>((resolve, reject) => {
    try {
      let score = 0;
      // Result Logic: answers are converted to corresponding alphabet using base 36
      // Then compare to correct answer
      answers.map((answer, i) => {
        if (numberToLetter(answer) === questions[i].answer) score++;
        return null;
      });

      resolve(score);
    } catch (error) {
      console.error("Error calculating result", error);
      reject(0);
    }
  });
}
