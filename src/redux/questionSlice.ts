import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import update from "immutability-helper";
import { QuestionProps } from "../helper/interface";

export interface AuthState {
  progress: number;
  questions: QuestionProps[];
  answers: Array<number>;
  selectedAnswer: number;
  result: number;
}

const initialState: AuthState = {
  questions: [],
  progress: 0,
  answers: [],
  selectedAnswer: -1,
  result: -1,
};

export const questionSlice = createSlice({
  name: "question",
  initialState,
  reducers: {
    setQuestion: (state, action: PayloadAction<QuestionProps[]>) => {
      state.questions = action.payload;
    },
    setSelectedAnswer: (state, action: PayloadAction<number>) => {
      state.selectedAnswer = action.payload;
    },
    nextQuestion: (state) => {
      state.answers = update(state.answers, {
        [state.progress]: { $set: state.selectedAnswer },
      });

      // If we're on the last question, Return from here.
      if (state.progress === state.questions.length - 1) return;

      let position = state.progress + 1;
      state.selectedAnswer = state.answers[position];
      state.progress = position;
    },
    prevQuestion: (state) => {
      state.answers = update(state.answers, {
        [state.progress]: { $set: state.selectedAnswer },
      });

      let position = state.progress - 1;
      state.selectedAnswer = state.answers[position];
      state.progress = position;
    },
    setResult: (state, action: PayloadAction<number>) => {
      state.result = action.payload;
    },
    restartTest: (state) => {
      state.progress = 0;
      state.answers = [];
      state.result = -1;
      state.selectedAnswer = -1;
    },
  },
});

// `useSelector((state: RootState) => state.counter.value)`;
// export const selectProgress = (state: RootState) => state.question.progress;
export const {
  setQuestion,
  nextQuestion,
  prevQuestion,
  setSelectedAnswer,
  setResult,
  restartTest,
} = questionSlice.actions;

export default questionSlice.reducer;
