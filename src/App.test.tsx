import { render, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";
import App from "./App";
import { store } from "./redux/store";

test("renders whole App and Navigation", async () => {
  const { getByText, getByTestId, queryByTestId } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  const user = userEvent.setup();

  //Landing page check.
  //Check that Personality test is showing on screen.
  expect(getByText(/Personality test/i)).toBeInTheDocument();

  //Click Start Button to navigate to "/test".
  await user.click(getByTestId("start_btn"));

  //Verify that current screen is /test by checking "test-screen" is currently showing
  expect(getByTestId("test-screen")).toBeVisible();

  //Verify questions are loading from mock query
  expect(getByTestId("question-loading")).toBeVisible();

  //Logic: Question container is not displayed while loading
  expect(queryByTestId("question-container")).toBeFalsy();

  //Question Container show up after loading
  await waitFor(() => getByTestId("question-container"));

  //Verify Question Loading has stopped showing
  expect(queryByTestId("question-loading")).toBeFalsy();

  //First Question: Verify Next Button is visible while Previous Button is not.
  expect(getByTestId("next-btn")).toBeVisible();
  expect(queryByTestId("previous-btn")).toBeFalsy();

  //Verify Next Button is Disabled
  expect(getByTestId("next-btn")).toHaveAttribute("disabled");

  //Click Option A
  await user.click(getByTestId("question-option0"));

  //Verify Next Button is now Enabled
  expect(getByTestId("next-btn")).not.toHaveAttribute("disabled");

  //Click Next Button
  await user.click(getByTestId("next-btn"));

  // Verify Previous button is now shown
  expect(getByTestId("previous-btn")).toBeVisible();

  // Go through remaining 4 question
  for (let i = 0; i < 4; i++) {
    await user.click(getByTestId("question-option1"));

    //Click Next Button
    await user.click(getByTestId("next-btn"));

    //Last Question
    // console.log(i);

    if (i === 2) {
      expect(getByText(/Finish Test/i)).toBeInTheDocument();
    }
  }

  //Verify that current screen is /result by checking "result-screen" is currently showing
  expect(getByTestId("result-screen")).toBeVisible();
});
