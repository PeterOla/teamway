import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Button } from "../../component/Button";
import { calculateResult } from "../../redux/actions";
import { useAppDispatch } from "../../redux/hooks";
import { restartTest, setResult } from "../../redux/questionSlice";
import { RootState } from "../../redux/store";
import { StyledH4 } from "../../styles/global";
import { Container } from "./styles";

function Result() {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { answers, questions, result } = useSelector(
    (state: RootState) => state.question
  );

  useEffect(() => {
    calculateResult(answers, questions).then((score) => {
      dispatch(setResult(score));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //   Result Logic: Condition based on user getting 2
  return (
    <Container data-testid={"result-screen"}>
      <StyledH4 align="center">{"Your Result"}</StyledH4>
      <StyledH4 fontSize="2.5em" align={"center"}>
        {result < 2
          ? "You are more of a public introvert and private extrovert"
          : "You are more of an extrovert"}
      </StyledH4>

      <Button
        primary
        autoFlex
        width={"40%"}
        onClick={() => {
          dispatch(restartTest());
          navigate("/");
        }}
      >
        Retake Test
      </Button>
    </Container>
  );
}

export default Result;
