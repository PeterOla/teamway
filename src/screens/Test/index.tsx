import React from "react";
import { useEffect } from "react";
import Main from "../../component/Main";
import { fetchQuestion } from "../../redux/actions";
import { useAppDispatch } from "../../redux/hooks";
import { setQuestion } from "../../redux/questionSlice";
import Header from "../../component/Header";
import { Container } from "./styles";

function Test() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    // Mock server query to get Questions
    fetchQuestion().then((val) => {
      dispatch(setQuestion(val));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container data-testid={"test-screen"}>
      <Header />
      <Main />
    </Container>
  );
}

export default Test;
