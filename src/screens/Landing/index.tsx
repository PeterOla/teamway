import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../../component/Button";
import { StyledH4 } from "../../styles/global";
import { Container } from "./styles";

function Landing() {
  let navigate = useNavigate();

  return (
    <Container>
      <StyledH4 width="auto">{"Personality test"}</StyledH4>

      {/* // Primary used to handle button design type. Filled  */}
      <Button
        data-testid={"start_btn"}
        primary
        autoFlex
        width={"40%"}
        onClick={() => navigate("test")}
      >
        Start
      </Button>
    </Container>
  );
}

export default Landing;
