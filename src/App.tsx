import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import GlobalStyle from "../src/styles/global";
import Landing from "./screens/Landing";
import Result from "./screens/Result";
import Test from "./screens/Test";

function App() {
  return (
    <>
      {/* Global Styling */}
      <GlobalStyle />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="test" element={<Test />} />
          <Route path="result" element={<Result />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
