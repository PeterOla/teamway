import styled, { createGlobalStyle } from "styled-components";

//Fonts used on reference website
export const fonts = {
  default: "DM Sans, Helvetica Neue, helvetica, arial, sans-serif",
  heading_ls:
    "Lora, timesnewroman, Times New Roman, times, baskerville, georgia, serif",
};

export const colors = {
  gray: "#EFEFEF",
  gray_40: "#f5f7f9",
  gray_80: "#ccd6e0",
  gray_100: "#26313e",
  white: "#ffffff",
  primary: "#26313e",
  secondary: "#ec6584",
};

export default createGlobalStyle`
* {
  box-sizing: border-box;
  font-family: ${fonts.default};
}
`;

type StyledTextProps = {
  width?: string;
  fontSize?: string;
  fontStyle?: string;
  align?: string;
  light?: boolean;
};

export const StyledH4 = styled.h4<StyledTextProps>`
  font-size: ${(props) => props.fontSize || "1.2em"};
  width: ${(props) => props.width || "100%"};
  font-style: ${(props) => props.fontStyle || "normal"};
  font-family: ${fonts.heading_ls};
  text-align: ${(props) => props.align || "auto"};
  color: ${colors.gray_100};
`;

export const StyledP = styled.span<StyledTextProps>`
  font-size: ${(props) => props.fontSize || "0.8em"};
  width: ${(props) => props.width || "100%"};
  font-style: ${(props) => props.fontStyle || "normal"};
  color: ${(props) => (props.light ? colors.white : colors.gray_100)};
`;

export const StyledFlex = styled.div`
  width: 100%;
  display: flex;
`;

type SpacerProps = {
  space?: string;
};

export const Spacer = styled.span<SpacerProps>`
  height: ${(props) => props.space || "20px"};
`;

export const SpacerHorizontal = styled.span<SpacerProps>`
  width: ${(props) => props.space || "20px"};
`;
