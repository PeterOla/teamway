##  Run Code
- `npm install && npm start` to start the app.

## External libraries
immutability-helper\
    - Mutate a copy of data without changing the original source.\
    - It makes maintaining immutable data structures easier and more efficient.\

styled-component\
    - Makes Components Less Bulky\
    - Easy to test\




